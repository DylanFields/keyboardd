// • We need to determine which arrow key was pressed.
// • We need to get access to the image box.
// • We need to know how to change the position of the image box.
// • We need to change the position based on the arrow key direction.
let boxTop = 200;
let boxLeft = 200;

document.addEventListener('keydown', logKey);

function logKey(e) {

    switch (e.code) {

        // left arrow pressed
        case "ArrowLeft":
            {
                boxLeft -= 10
            };
            break;
            // up arrow pressed
        case "ArrowUp":
            {
                boxTop -= 10
            };
            break;
            // right arrow pressed
        case "ArrowRight":
            {
                boxLeft += 10
            };
            break;
            // down arrow pressed
        case "ArrowDown":
            {
                boxTop += 10
            };
            break;
    }
    console.log(e)
    let box = document.getElementById("box")
    box.style.top = boxTop + "px";
    box.style.left = boxLeft + "px";
};